<?php

namespace Drupal\prepared_data\Storage;

/**
 * Class StorageException
 */
class StorageException extends \RuntimeException {}
