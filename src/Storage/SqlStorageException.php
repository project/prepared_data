<?php

namespace Drupal\prepared_data\Storage;

/**
 * Class SqlStorageException.
 */
class SqlStorageException extends StorageException {}
